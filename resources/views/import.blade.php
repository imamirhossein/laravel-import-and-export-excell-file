<!DOCTYPE html>
<html>
<head>
    <title>برون‌ریزی و درون‌ریزی داده از پروندهٔ اکسل در لاراول</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

    <style>
        body
        {
            direction: rtl;
            text-align: right;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            برون‌ریزی و درون‌ریزی داده از پروندهٔ اکسل در لاراول۷
        </div>
        <div class="card-body">
                @include('messages')
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" class="form-control">
                <br>
                <button class="btn btn-success">درون‌ریزی داده‌های کاربر</button>
                <a class="btn btn-warning" href="{{ route('export') }}">برون‌ریزی داده‌های کاربر</a>
            </form>
        </div>
    </div>
</div>

</body>
</html>
