<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function index()
    {
        return view('import');
    }

    public function import()
    {
        Excel::import(new UsersImport, request()->file('file'));
        $msg = 'درون‌ریزی با موفقیت انجام شد.';
        return \back()->with('success', $msg);
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
